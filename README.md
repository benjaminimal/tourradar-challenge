# TourRadar

## Associate Software Engineer (Backend) Test Case
## Collecting operator confirmations
### Context
To make sure that travellers have the best experience with their queries answered in a timely fashion we have a 24/7 Customer Support (CS) department. 
The CS Team is responsible for communicating between operators and customers to make sure that the customers are satisfied, well informed and have all necessary documents to proceed with their bookings for their tour of a lifetime.

The lifecycle of booking a tour is as follows:
- The traveller finds her favorite tour and makes an enquiry (e.g. booking).
- A BCP (Booking Conversation Page) is created and the operator receives an email with the customer details.
- The operator confirms the booking on their side. 
- The operator sends a confirmation email to tourradar email account.
- A CS agent picks up the email from the tourradar email, saves the email as PDF and attaches the confirmation to the BCP.

From here on the customer and the agent may carry out a conversation for additional services or information that they might need.

**An enquiry has following data**:
- id
- first name
- last name
- date of birth
- passenger count
- email
- price of the tour
- files (with name, size, path and upload date)

### CS Team needs your help
As you can imagine saving emails as PDF and uploading them somewhere is a tedious and time-consuming task, which can easily be automated. 
CS team has requested that you help them by creating a solution to automate and reduce this workload on their side.

#### What can you do to help?
Create a small project which has following:
- a console script to generate PDF from emails, and add it to the System (updating it in the database and saving file locally somewhere).
- a page that shows uploaded files provided an enquiry ID, with details such as size, upload date, and an option to download the file. This can be a basic page with a table of uploaded files against an ID. _You are not required to create a good front-end design._
- Include a README on how to setup/configure to be able to run your solution on our local  environment. 

For the implementation you can choose any programming language and framework you prefer. 

#### Sample Data
To help you quickly bootstrap the project we have added sample data for:
- enquiries inside enquiries/sample.csv 
- emails inside emails directory as html files

### Questions we ask you to answer:
- How will you automate the process of checking new emails in tourradar's inbox and upload new files instead of running the console script manually?
- How would you ensure your solution works as expected?
- How would you secure your solution?

## When is this test case due?
You should submit the solution to this testcase within 72 hours.

## How to ask questions?
Is there something ambiguous in the test case? Some of the ambiguity has been intentionally left in there to give room to your creativity.

If there is a question that appears to be a blocker, please head over to issues and create a New Issue. Make sure to assign the newly created issue to one of the available options (except tourradar-testcases). Being explicit in your question will help us provide better and more contextual answers.

## How to submit?
- Create a new branch in this repository.
- Add your solution in a subdirectory; say *solution* in that branch.
- Push the branch to this repository.
- Create a new Pull Request for your branch against `master`.
- When creating a Pull Request, please add all the reviewers (except tourradar-testcases) from the "Reviewers" panel on the right hand side.

## How does the review process happen?
- One or more TourRadar Senior Engineers will go through your solution and leave their feedback.
- Based on our evaluation of your solution, a decision will be made about whether to advance you in the progress.
- Once the solution has been reviewed and evaluated, our Recruiting team will get back to you with next steps.
