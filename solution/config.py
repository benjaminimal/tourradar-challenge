import os

from dotenv import load_dotenv

load_dotenv()

base_dir = os.path.dirname(__file__)


class Config:
    DEBUG = os.environ.get('DEBUG', False)
    TESTING = os.environ.get('TESTING', False)
    SECRET_KEY = os.environ['SECRET_KEY']
    BASE_DIR = base_dir
    APP_TITLE = 'Enquiry confirmations'

    # Database
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URI', 'sqlite:///{}'.format(os.path.join(base_dir, 'data.db')))
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # File storage
    UPLOAD_DIR = os.environ.get('UPLOAD_DIR', os.path.join(base_dir, 'app/uploads'))

    # Pagination
    PER_PAGE = os.environ.get('PER_PAGE', 20)
