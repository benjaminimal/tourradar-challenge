# Operator confirmation collection

A web application with command line interface to help the Customer Service of tourradar to convert, upload and retrieve confirmation emails for enquiries.

## Installation

__Requrirements__: __Python3__, __pip__

1. Clone the repository and cd into the project directory.

    ```
    $ git clone https://gitlab.com/benjaminimal/tourradar-challenge.git
    $ cd tourradar-challenge/solution
    ```
    
2. Install and setup python virtualenv

    ```
    $ pip install virtualenv
    $ virtualenv -p $(which python3) .venv
    $ source .venv/bin/activate
    ```
   
3. Install requirements

    ```
    $ pip install -r requirements.txt
    ```

4. Install wkhtmltopdf

    - Debian/Ubuntu  
        ```
        $ sudo apt install wkhtmltopdf
        ```

    - macOS
        ```
        $ brew install caskroom/cask/wkhtmltopdf
        ```

    - or [download from the official site](https://wkhtmltopdf.org/downloads.html)

5. Set up configuration as explained in [this secion](#configuration)

6. Initialize the database
    
    ```
    $ flask init_db
    ```
    
8. (Optional) You can setup some sample data

    ```
    $ flask fill_db <number_of_enquiry_confirmations_to_generate>
    ```

## Configuration

There are some parameters which have to be set in a `.env` file placed in directory where the `wsgi.py` file lies.
You can look at the included `.env.example` file for reference.

- `SECRET_KEY` The secret key used for cryptographic signing of cookies. Make sure this is long, random and kept secret.
- `UPLOAD_DIR` The directory where the generated pdf files are to be stored.

## Usage
First make sure you are in the `solution` directory of the repository, where your `wsgi.py` file lives, and activate your virtual environment like this.
```
$ source .venv/bin/activate
```

To convert and save a local html or text file execute the following.
```
$ flask save_email /path/to/email.html 1
```
You can find samples to play around with under `../enquiries/`.  
Note that the second argument `1` defines the enquiry id the email will be associated to.

You can run the application locally and serve the web page to filter and retrieve the generated files like this.
```
$ gunicorn -b localhost:8080 wsgi:app
```
Now you can reach the front end at http://locahost:8080.  
To serve the application on your network you can [configure gunicorn](http://docs.gunicorn.org/en/stable/configure.html) to you needs and hook it up to the reverse proxy of your choice.

## Answers to assignment questions

1. How will you automate the process of checking new emails in tourradar's inbox and upload new files instead of running the console script manually?
    
    I would extend the application to fetch emails from configurable imap inbox with [imaplib](https://docs.python.org/3/library/imaplib.html) and poll this periodically with [appscheduler](https://apscheduler.readthedocs.io/en/latest/userguide.html).
    Further I would have to automate the process of filtering for relevant emails only and mapping them to enquiries.
    This I would do by parsing the email headers and body and match it with existing enquiries.
    
2. How would you ensure your solution works as expected?
    
    - Write automated tests for core functionality.
    - Ask for more sample data and run manual tests on different environments.
    
3. How would you secure your solution?
    
    - Disable local file access when converting emails.
    - Disable JavaScript execution when converting emails.
    - Do more research on PDF attacks to learn more about the potential vulnerabilities and countermeasures.
