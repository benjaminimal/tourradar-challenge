import os
from uuid import uuid4

from flask import current_app as app

import pdfkit

from . import db
from .models import Enquiry, EnquiryConfirmation


def save_email(email: str, enquiry: Enquiry) -> None:
    """
    Save the email as a pdf to the file system and database.

    :param email: The email to save
    :param enquiry: The enquiry the email is associated to
    """
    options = {
        'quiet': '',
        'user-style-sheet': os.path.join(app.static_folder, 'css/print.css')
    }
    pdf = pdfkit.from_string(email, False, options=options)
    filename = generate_pdf_filename()
    size = len(pdf)

    enquiry_confirmation = EnquiryConfirmation(**{
        'name': filename,
        'blob': pdf,
        'size': size,
        'enquiry': enquiry
    })
    db.session.add(enquiry_confirmation)
    db.session.commit()

    path = os.path.join(app.config['UPLOAD_DIR'], filename)
    with open(path, 'wb') as f:
        f.write(pdf)


def generate_pdf_filename() -> str:
    """
    Generate a unique file name for a pdf.

    :return: A unique file name
    """
    return '{}.pdf'.format(uuid4().hex)
