from io import BytesIO

from flask import Blueprint, render_template, send_file, request
from flask import current_app as app

from .models import Enquiry, EnquiryConfirmation

app_blueprint = Blueprint('app', __name__)


@app_blueprint.route('/')
def get_confirmations():
    """
    Show a paginated table of enquiry confirmations with the option to filter
    by enquiry id.

    :url-param pk: The enquiry id to query for
    :url-param page: The number of the page to get
    """
    pk = request.args.get('pk', default=None, type=int)
    page = request.args.get('page', default=1, type=int)
    per_page = app.config['PER_PAGE']
    if pk:
        query = EnquiryConfirmation.query.join(Enquiry).filter(Enquiry.id == pk)
    else:
        query = EnquiryConfirmation.query
    confirmations = query \
        .order_by(EnquiryConfirmation.created_at.desc()) \
        .paginate(page, per_page, error_out=False)
    return render_template(
        'index.html',
        pk=pk,
        confirmations=confirmations
    )


@app_blueprint.route('/download/<int:pk>')
def download_confirmation(pk):
    """
    Download an enquiry confirmation pdf file by its id.

    :param pk: The id of the enquiry confirmation
    """
    confirmation = EnquiryConfirmation.query.get(pk)
    filename = confirmation.name
    file = BytesIO(confirmation.blob)
    return send_file(
        file,
        attachment_filename=filename,
        mimetype='application/pdf',
        as_attachment=True
    )
