import os
import sys
import random
from datetime import datetime

from flask import current_app as app
from flask.cli import with_appcontext

import click
from sqlalchemy.exc import SQLAlchemyError

from . import db
from .models import Enquiry
from .core import save_email


@click.command()
@with_appcontext
def echo_config():
    """
    Print the applications configuration. Exists for debugging purposes.
    """
    for key, val in app.config.items():
        print(key, val)


@click.command()
@with_appcontext
def init_db():
    """
    Initialize database tables.
    """
    db.create_all()


@click.command()
@click.argument('count', type=int)
@with_appcontext
def generate_test_data(count):
    """
    Fill the database with sample data.

    :param count: The number of enquiry confirmations to generate
    """
    test_data_dir = os.path.join(app.config['BASE_DIR'], 'test/data')
    enquiries_file = os.path.join(test_data_dir, 'enquiries/sample.csv')

    with open(enquiries_file, 'r') as f:
        f.readline()
        entries = [x for x in f.readlines()]

    for entry in entries:
        entry = [x.strip().strip('"') for x in entry.split(',')]
        enquiry = Enquiry(**{
            'id': int(entry[0]),
            'first_name': entry[1],
            'last_name': entry[2],
            'date_of_birth': datetime.strptime(entry[3], '%Y/%m/%d').date(),
            'passenger_count': int(entry[4]),
            'email': entry[5],
            'price': int(float(entry[6]) * 100)
        })
        db.session.add(enquiry)
    try:
        db.session.commit()
    except SQLAlchemyError as e:
        db.session.rollback()
        print(e)
        sys.exit(1)

    email_dir = os.path.join(test_data_dir, 'emails')
    pdfs = [open(os.path.join(email_dir, x), 'r').read() for x in os.listdir(email_dir)]
    enquiries = Enquiry.query.all()
    [save_email(pdfs[n % len(pdfs)], random.choice(enquiries)) for n in range(count)]


@click.command()
@click.argument('src', type=click.File('r'))
@click.argument('enquiry_id', type=int)
@with_appcontext
def save_confirmation_email(src, enquiry_id):
    """
    Generate a pdf from email and save it to the file system and database.

    :param src: The path to the email html or text file
    :param enquiry_id: The id of the enquiry the email is associated to
    """
    enquiry = Enquiry.query.get(enquiry_id)
    if enquiry is None:
        print("Couldn't save email: Enquiry with id {} not found".format(enquiry_id))
        sys.exit(1)
    email = src.read()
    save_email(email, enquiry)
