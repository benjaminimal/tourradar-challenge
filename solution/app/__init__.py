import os

from flask import Flask

from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


def create_app():
    app = Flask(__name__)

    app.config.from_object('config.Config')

    if not os.path.exists(app.config['UPLOAD_DIR']):
        os.makedirs(app.config['UPLOAD_DIR'])

    db.init_app(app)

    from .views import app_blueprint
    app.register_blueprint(app_blueprint)

    return app
