import os

from flask import current_app as app

from hurry.filesize import size as human_friendly_size

from . import db


class BaseModel(db.Model):
    """
    Abstract base model.
    """
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=db.func.current_timestamp())
    updated_at = db.Column(db.DateTime, default=db.func.current_timestamp(), onupdate=db.func.current_timestamp)


class Enquiry(BaseModel):
    """
    An enquiry made by a traveller.
    """
    first_name = db.Column(db.String(120), nullable=False)
    last_name = db.Column(db.String(120), nullable=False)
    date_of_birth = db.Column(db.Date, nullable=False)
    passenger_count = db.Column(db.Integer, nullable=False)
    email = db.Column(db.String(120), nullable=False)
    price = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return '<Enquiry {} {} {}>'.format(self.id, self.first_name, self.last_name)


class EnquiryConfirmation(BaseModel):
    """
    A confirmation for an enquiry in pdf format.
    """
    name = db.Column(db.String(255), unique=True, nullable=False)
    blob = db.Column(db.LargeBinary(), nullable=False)
    size = db.Column(db.Integer, nullable=False)

    enquiry_id = db.Column(db.Integer, db.ForeignKey('enquiry.id'), nullable=False)
    enquiry = db.relationship('Enquiry', backref=db.backref('confirmations', lazy=True))

    def __repr__(self):
        return '<PdfFile {}, {}>'.format(self.id, self.name)

    @property
    def path(self):
        return os.path.join(app.config['UPLOAD_DIR'], self.name)

    @property
    def upload_date(self):
        return self.created_at.strftime('%Y-%m-%d')

    @property
    def filesize(self):
        return human_friendly_size(self.size)
