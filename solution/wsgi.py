from app import create_app
from app import commands as cmd

app = create_app()

app.cli.add_command(cmd.echo_config, 'echo_config')
app.cli.add_command(cmd.init_db, 'init_db')
app.cli.add_command(cmd.generate_test_data, 'fill_db')
app.cli.add_command(cmd.save_confirmation_email, 'save_email')

if __name__ == '__main__':
    app.run()
