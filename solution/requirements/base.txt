# https://click.palletsprojects.com/en/7.x/
Click==7.0

# http://flask.pocoo.org/docs/1.0/
Flask==1.0.3

# https://itsdangerous.palletsprojects.com/en/1.1.x/
itsdangerous==1.1.0

# http://jinja.pocoo.org/
Jinja2==2.10.1

# https://markupsafe.palletsprojects.com/en/1.1.x/
MarkupSafe==1.1.1

# https://werkzeug.palletsprojects.com/en/0.15.x/
Werkzeug==0.15.4

# https://github.com/theskumar/python-dotenv
python-dotenv==0.10.3

# https://flask-sqlalchemy.palletsprojects.com/en/2.x/
Flask-SQLAlchemy==2.4.0

# https://docs.sqlalchemy.org/en/13/
SQLAlchemy==1.3.4

# https://github.com/JazzCore/python-pdfkit
pdfkit==0.6.1

# https://pypi.org/project/hurry.filesize/
hurry.filesize==0.9
